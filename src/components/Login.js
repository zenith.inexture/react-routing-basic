import React from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "./auth";

function Login() {
  const [user, setUser] = React.useState();
  const auth = useAuth();
  const navigate = useNavigate();

  const handlelogin = () => {
    auth.login(user);
    navigate("/profile", { replace: true });
  };
  return (
    <>
      <label>
        Username :
        <input type="text" onChange={(e) => setUser(e.target.value)}></input>
      </label>
      <button onClick={handlelogin}>Login</button>
    </>
  );
}

export default Login;
