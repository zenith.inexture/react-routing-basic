import { NavLink, Outlet } from "react-router-dom";

function Products() {
  return (
    <>
      <div>Product page</div>
      <NavLink to="product1" style={{ margin: "10px" }}>
        Product1
      </NavLink>
      <NavLink to="product2">Product2</NavLink>
      {/* if we want to render child route then we have to use Outlet for rendering product1 and product2 page we have to use Outlet */}
      <Outlet />
    </>
  );
}

export default Products;
