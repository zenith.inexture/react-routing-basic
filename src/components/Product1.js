import React from "react";
import { NavLink, Outlet } from "react-router-dom";

function Product1() {
  return (
    <>
      <div>Product 1 page</div>
      <NavLink to="product11">Product11</NavLink>
      <NavLink to="product12">Product12</NavLink>
      <Outlet />
    </>
  );
}

export default Product1;
