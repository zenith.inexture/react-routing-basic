import { useNavigate } from "react-router-dom";

const Home = () => {
  //useNevigate hook use for the navigate to another page by onclick or something
  //this hooks returns the function
  const clicknavigate = useNavigate();
  return (
    <>
      <div>Home Page</div>
      <button
        onClick={() => {
          clicknavigate("success");
        }}
      >
        Yes??
      </button>
    </>
  );
};
export default Home;
