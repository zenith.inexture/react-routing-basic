import { useNavigate } from "react-router-dom";

const Success = () => {
  //useNevigate returns a function so we can call onclick function clicknevigate
  const clicknavigate = useNavigate();
  return (
    <>
      <div>Work done Successfully</div>
      <button
        onClick={() => {
          clicknavigate("/");
          //   we can write -1 also for go back to previous route
        }}
      >
        Back
      </button>
    </>
  );
};
export default Success;
