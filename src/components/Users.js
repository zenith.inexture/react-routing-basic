import React from "react";
import { Outlet, useSearchParams } from "react-router-dom";

function Users() {
  //useSearchParams hook is semilar to the useState hook in react
  //it's returns the object and one change function
  const [searchparams, setSearchParams] = useSearchParams();

  //.get method fetch the value of object by passing the key like filter and return the true or false
  const checksearchparams = searchparams.get("filter") === "active";
  return (
    <>
      <p>user 1</p>
      <p>user 2</p>
      <p>user 3</p>
      {/* for rendering the child route of the users route  */}
      <Outlet />
      <div>
        {/* in onclick we set the value of searchparams by filter : "active"  */}
        <button onClick={() => setSearchParams({ filter: "active" })}>
          Active Users
        </button>
        <button onClick={() => setSearchParams({})}>Remove filter</button>
      </div>
      {checksearchparams ? <div>All Active users</div> : <div>All Users</div>}
    </>
  );
}

export default Users;
