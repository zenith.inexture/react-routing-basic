import { NavLink } from "react-router-dom";
import { useAuth } from "./auth";
const Navbar = () => {
  const auth = useAuth();
  return (
    <div className="navbar">
      {/* we can use NavLink over Link also - but in Link does't provide the active class but the NavLink provides active class */}
      <NavLink to="/">
        {/* normally work as html a tag  */}
        Home
      </NavLink>
      <NavLink to="about">About</NavLink>
      <NavLink to="products">Products</NavLink>
      <NavLink to="users">Users</NavLink>
      <NavLink to="profile">Profile</NavLink>
      {!auth.user && <NavLink to="login">Login</NavLink>}
    </div>
  );
};

export default Navbar;
