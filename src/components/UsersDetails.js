import { useParams } from "react-router-dom";

function UsersDetails() {
  //if we want the params name then we have to use useParams hook
  // useParams hook is returns the object of key value pair
  const useparams = useParams();
  const userid = useparams.userID;
  return <div>Details of User {userid}</div>;
}

export default UsersDetails;
