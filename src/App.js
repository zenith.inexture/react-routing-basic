import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import Navbar from "./components/Navbar";
import "./App.css";
import Success from "./components/Success";
import Nomatch from "./components/Nomatch";
import Products from "./components/Products";
import Product1 from "./components/Product1";
import Product2 from "./components/Product2";
import Users from "./components/Users";
import UsersDetails from "./components/UsersDetails";
import Admin from "./components/Admin";
import Profile from "./components/Profile";
import { AuthProvider } from "./components/auth";
import Login from "./components/Login";
import { RequireAuth } from "./components/RequireAuth";
import Product11 from "./components/Product11";
import Product12 from "./components/Product12";

//with lazy load - lazy returns the function which contain the import the file
const LazyAbout = React.lazy(() => import("./components/About"));
// import About from "./components/About";

function App() {
  return (
    <AuthProvider>
      <Navbar />
      {/* the Link of Navbar is start finding the route releted to path  */}
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route
          path="about"
          element={
            //in dev tools if you want to see the Loading then try to slow the network speed
            //if lazy component not wrapped into the React.Suspense then it's gives us the error
            <React.Suspense fallback="Loading...">
              <LazyAbout />
            </React.Suspense>
          }
        ></Route>
        <Route path="success" element={<Success />}></Route>
        <Route path="products" element={<Products />}>
          {/* Nested Route helps to forms the path for child like localhost:3000/products/product1 */}

          {/* if we want to render any component on the perent path then we can use the index route  */}
          <Route index element={<Product1 />} />

          <Route path="product1" element={<Product1 />}>
            {/* child in child so we have to use outlet in Product1 Page then we can see the Product11 and Product12 component  */}
            <Route path="product11" element={<Product11 />}></Route>
            <Route path="product12" element={<Product12 />}></Route>
          </Route>
          <Route path="product2" element={<Product2 />}></Route>
        </Route>

        <Route path="users" element={<Users />}>
          {/* it is deficult to write 100 or 1000 route in our app so we can use dynamic routes  */}
          {/* in this useID is dynamic route path - path is users/1 , users/zenith anything*/}
          {/* this userID is known as URL params  */}
          <Route path=":userID" element={<UsersDetails />}></Route>
          {/* first is start finding the match of the path if not found then render the URL params component  */}
          <Route path="admin" element={<Admin />}></Route>
        </Route>

        <Route
          path="profile"
          element={
            <RequireAuth>
              <Profile />
            </RequireAuth>
          }
        ></Route>
        <Route path="login" element={<Login />}></Route>
        {/* if no page matches found then...  */}
        <Route path="*" element={<Nomatch />}></Route>
      </Routes>
    </AuthProvider>
  );
}

export default App;
